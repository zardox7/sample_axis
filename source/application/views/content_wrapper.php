<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<!–[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]–>
<!–[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]–>
<!–[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]–>
<!–[if gt IE 8]><!–> <html lang="en"> <!–<![endif]–>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Axis Labs - Pathology Lab Reporting System</title>
<!–[if IE 9]>
<style type="text/css"> .gradient { filter: none;} </style>
<![endif]–>
<?php 
	echo assets_css($stylesheets); 
	echo assets_js($javascripts); 
?>
</head>
<body>
  <?php echo $content; ?>
</body>
</html>





