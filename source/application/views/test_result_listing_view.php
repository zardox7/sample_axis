<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row headerbg">
		<div class="col-md-2">
			<img src="/assets/images/axis_logo_tbg.png" alt="logo" width="96" height="52" />
		</div>
		<div class="col-md-10">
			<h3>Axis Labs - Pathology Lab Reporting</h3>
		</div>
	</div>
	<div class="row" style="padding-top:20px;padding-left:15px;">
		<div class="lead">Lab Results for <?php echo $name_first.' '.$name_last ?></div>
		<?php 
			if($this->session->flashdata('errors')) {
				echo '<div class="text-danger">'.$this->session->flashdata('errors').'</div>';
			}
		?>
	</div>
	<div class="well" style="padding-top:0;">
		<div class="row">
			<div class="col-md-3">
				<h4><span class="label label-default">Test Results</span></h4>
			</div>
			<div class="col-md-9">
				<?php 
					if( $num_test_results === false) {
						echo '<p class="text-danger">You have no test results to report';
						if(! $date_range_results == '999') {
							echo ' within the time frame specified';
						}
						echo '.</p>';
					}
					echo form_open('patient_con/get_results/'.$patient_pass_code, ['class' => 'form-inline', 'method' => 'post', 'role' => 'form']);
						echo '<div class="form-group">'."\n";
							echo '<label for="date_range_results" class="sr-only">Date Range</label>';
							echo form_dropdown('date_range_results',['999' => 'all','1' => 'previous month','6' => 'previous six months','12' => 'previous year'],$date_range_results,' class="form-control"');
						echo "</div>\n";
						echo '<div class="form-group">'."\n";
							echo form_input(['class' => 'btn btn-success','name' => 'get_form','type' => 'submit','value' => 'Filter']);
						echo "</div>\n";
					echo form_close();
				?>
			</div>
		</div>
		<?php 
			if( $num_test_results != false) {
				echo '<div class="row">';
				echo $this->table->generate(); 		
				echo '</div>';
			}
		?>
		<div class="row">
			<div class="col-md-6">
			<h4><span class="label label-default">Patient Information</span></h4>
			<table class="table table-bordered table-condensed">
			<tbody>
			<tr>
				<td>Name</td><td><?php echo $name_first.' '.$name_last ?></td>
			</tr>
			<tr>
				<td>Date birth</td><td><?php echo date('M j, Y',strtotime($date_birth)) ?></td>
			</tr>
			<tr>
				<td>Address</td>
				<td>
					<?php echo $address_block; ?>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
