<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('phpmailer_helper')){
	function phpmailer_helper() {
		require_once('PHPMailer/PHPMailerAutoload.php');
		define ('PHPMAILER_PORT', 2525);
		define ('PHPMAILER_HOST', 'localhost');
		define ('PHPMAILER_FROM_EMAIL', 'MAIL@zardox.net');
		define ('PHPMAILER_FROM_NAME', 'admin');
		define ('PHPMAILER_SMTPAUTH', false);
		define ('PHPMAILER_USERNAME', '');
		define ('PHPMAILER_PASSWORD', '');
	}
}
