<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Wrapper class that extends the CodeIgniter framework (CI), allows for insertion of header and footer
 * The MY_Controller.php is part of the CI that is defined to run just after the CI core 
 *
 * @package Core template
 * @author 	Michael Collins
 * @link	http://sample_axis.zardox.net/
 * @codeCoverageIgnore
 */
class MY_Controller extends CI_Controller {

	/**
	 * Instance variables of the parent class defines the resources to appear on every page
	 * The local_stylesheets and local_javascripts are defined on controllers that extend this class, and allow specific resources to be defined for those view pages
	 *
	*/
	protected $stylesheets = ['bootstrap.css','site.css']; 
	protected $javascripts = ['jquery.js','bootstrap.js'];
	protected $local_stylesheets = [];
	protected $local_javascripts = [];
	 
	function __construct(){ 
		parent::__construct();
	}
	
	/**
	 * Passes on the intended HTML content for the requested page, and wraps a header and footer around that content
	 *
	 * @param content - the HTML that is to be inserted into the body tags of the template
	 * @return  output printed to the page    
	 */
	protected function render($content) { 
		$view_data = [
			'content' => $content,
			'stylesheets' => $this->get_stylesheets(),
			'javascripts' => $this->get_javascripts()
		];
		$this->load->view('content_wrapper',$view_data);
	}
		
	/**
	 * Gets the stylesheets and javascripts defined for the current page
	 *
	 * @param content - none
	 * @return  merged local and parent class instance variables    
	 */
	protected function get_stylesheets() {
		return array_merge($this->stylesheets,$this->local_stylesheets);
	}
		
	protected function get_javascripts() {
		return array_merge($this->javascripts,$this->local_javascripts);
	}

}
/*

 'sweetalert.css',
 'jquery-ui.css',
 
 'jquery-ui.js',
 'sweetalert.min.js', 
 'site_operator.js'

*/
 ?>