<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Patient User Models
 * 
 * @package Patient website
 * @author 	Michael Collins
 * @link	http://sample_axis.zardox.net/index.php/operator_con
 */
 
/**
 * Patient_mod class is for the Model, all functions within the Patient Website
 */
class Patient_mod extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	/**
	 * Compares form values to the values in the database to perform authentication 
	 *
	 * @param  string Email     
	 * @param  string Password     
	 * @return string Patient Pass Code (or false for failure) 
	 */
	function login_patient($name_last,$patient_pass_code) {
		$this->db->where(['name_last' => $name_last, 'patient_pass_code' => $patient_pass_code]);
		$query = $this->db->get('exp_mc_patient');
		if($query->num_rows() == 1){
			$this->session->set_userdata(['ID_patient' => $query->row()->ID_patient]);
			$this->session->set_userdata(['patient_pass_code' => $query->row()->patient_pass_code]);
			$this->input->set_cookie([
				'name'   => 'name_last',
				'value'  => $query->row(0)->name_last,
				'expire' => '86500',
				'path'   => '/'
			]); 
			$this->input->set_cookie([
				'name'   => 'patient_pass_code',
				'value'  => $query->row(0)->patient_pass_code,
				'expire' => '86500',
				'path'   => '/'
			]); 
			return $query->row(0)->patient_pass_code;
		}
		else{
			return false;
		}
	}	
			   
	/**
	 * Retrieve the data needed on the patient pages. 
	 *
	 * @param string patient passcode
	 * @return object The record data for the patient, as a CI object (or false for failure)
	 */
	function get_patient($patient_pass_code) {

		$this->db->select('name_first,name_last,date_birth,address_1, address_2,city,state,postal_code,country,patient_pass_code');
		$this->db->from('exp_mc_patient');
		$this->db->where(['patient_pass_code' => $patient_pass_code]);	
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$patient_arr = $query->row_array();
			$data['name_first']=$patient_arr['name_first'];
			$data['name_last']=$patient_arr['name_last'];
			$data['date_birth']=$patient_arr['date_birth'];
			$data['address_1']=$patient_arr['address_1'];
			$data['address_2']=$patient_arr['address_2'];
			$data['city']=$patient_arr['city'];
			$data['state']=$patient_arr['state'];
			$data['postal_code']=$patient_arr['postal_code'];
			$data['country']=$patient_arr['country'];
			$data['patient_pass_code']=$patient_arr['patient_pass_code'];
			return $data;
		}
		else{
			return false;
		}
	}

	/**
	 * Retrieve the data needed on the patient detail page. with a list of that patient's test results
	 *
	 * @param  string patient passcode 
	 * @param  integer the user entered parameter for the date range filter  
	 * @return object   - patient and test results record data, as a CI object (or false for failure)
	 */
	function get_patient_test_results($patient_pass_code,$date_range_results) {
		if( $date_range_results == '999'){
			$date_test_result_service = '';
		}
		else{
			$date_test_result_service = date('Y-m-d', strtotime(($date_range_results == '1') ? '-1 months' : (($date_range_results == '12') ? '-12 months' : '-6 months')));
		}
		$this->db->select('exp_mc_test_result.ID_test_result,exp_mc_test_result.date_test_result_request, exp_mc_test_result.date_test_result_service, exp_mc_staff.staff_name_first, exp_mc_staff.staff_name_last, exp_mc_staff.staff_location, exp_mc_lab.lab_code, exp_mc_lab.lab_name, exp_mc_lab.lab_location, exp_mc_test.test_name, exp_mc_test.test_code');
		$this->db->from('exp_mc_test_result');
		$this->db->join('exp_mc_patient','exp_mc_patient.ID_patient = exp_mc_test_result.FKID_patient');
		$this->db->join('exp_mc_test','exp_mc_test.ID_test = exp_mc_test_result.FKID_test');
		$this->db->join('exp_mc_staff','exp_mc_staff.ID_staff = exp_mc_test_result.FKID_staff_lab');
		$this->db->join('exp_mc_lab','exp_mc_lab.ID_lab = exp_mc_test_result.FKID_lab');
		$where_arr=['exp_mc_patient.patient_pass_code' => $patient_pass_code];
		if(! $date_test_result_service == ''){
			$where_arr=array_merge($where_arr, ['exp_mc_test_result.date_test_result_service >=' => $date_test_result_service]);
		}
		$this->db->where($where_arr);	
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query;
		}
		else{
			return false;
		}
	}

	/**
	 * Create a SQL statement with joins to find the patient, their test results, and staff and lab details, the data needed on the patient test result detail page
	 *
	 * @param   integer - ID of the test result record
	 * @return array  - patient and test result record data, the CI result object is translated to an array
	 */
	function get_a_test_result($ID_test_result) {

		$this->db->select('exp_mc_test_result.ID_test_result,exp_mc_test_result.date_test_result_request, exp_mc_test_result.date_test_result_service, exp_mc_test_result.test_result_diagnosis, exp_mc_staff.staff_name_first, exp_mc_staff.staff_name_last, exp_mc_staff.staff_location, exp_mc_lab.lab_code, exp_mc_lab.lab_name, exp_mc_lab.lab_location, exp_mc_test.test_name, exp_mc_test.test_code');
		$this->db->from('exp_mc_test_result');
		$this->db->join('exp_mc_patient','exp_mc_patient.ID_patient = exp_mc_test_result.FKID_patient');
		$this->db->join('exp_mc_test','exp_mc_test.ID_test = exp_mc_test_result.FKID_test');
		$this->db->join('exp_mc_staff','exp_mc_staff.ID_staff = exp_mc_test_result.FKID_staff_lab');
		$this->db->join('exp_mc_lab','exp_mc_lab.ID_lab = exp_mc_test_result.FKID_lab');
		$this->db->where(['exp_mc_test_result.ID_test_result' => $ID_test_result]);	
		$query = $this->db->get();
		if($query->num_rows() == 1){
			return $query->row_array();
		}
		else{
			return false;
		}
	}

	function send_test_result_email($ID_test_result,$patient_pass_code,$email_pdf) {
		$this->load->helper('phpmailer');
		phpmailer_helper();
		// Instantiate new PHPMailer object
		$mail = new PHPMailer;
		$mail->SMTPDebug = 0; // Enable verbose debug output, set to 1 or 2. 0 disables
		$mail->IsSMTP(); // Set mailer to use SMTP
        $mail->SMTPAuth = PHPMAILER_SMTPAUTH;
		if(PHPMAILER_SMTPAUTH === true){
            $mail->Username = PHPMAILER_USERNAME;
            $mail->Password = PHPMAILER_PASSWORD;				
		}
	    // $mail->Mailer = 'smtp';
        $mail->Host = PHPMAILER_HOST;
        $mail->Port = PHPMAILER_PORT;
		$mail->From = PHPMAILER_FROM_EMAIL;
		$mail->FromName = PHPMAILER_FROM_NAME;
        $mail->AddAddress($email_pdf, "Guest");
        $mail->Subject = "Axis Lab Test Result Report";
        $mail->Body ="Attached is the PDF copy of your lab results.";
					
		$curl=curl_init();   
		$t_filename=tempnam(sys_get_temp_dir(), 'pdf');
		$tmpfilehandle = fopen($t_filename, "w");
		curl_setopt($curl, CURLOPT_URL, site_url('patient_con/pdf/'.$ID_test_result.'/'.$patient_pass_code));
		curl_setopt($curl, CURLOPT_FILE, $tmpfilehandle);
		curl_exec($curl);
		curl_close($curl);
	   
	   $mail->AddAttachment($t_filename,$patient_pass_code.'_'.$ID_test_result.'.pdf');
	   fclose($tmpfilehandle);		
		
        if( $mail->Send() ){
			if($this->router->class == 'operator_con'){
				$this->set_email_flag_test_result($ID_test_result);
			}
 			return ['errors' => 'Email sent successfully to '.$email_pdf];
       }
		else{
			return ['errors' => 'Email Failed'];
		}
	}    


	/**
	 * Set flag to indicate an email was sent
	 *
	 * @param   integer - ID of the test result record
	 * @return bool - whether or not the db updating was a success
	 */
	function set_email_flag_test_result($ID_test_result) {
		  return $this->db->update('exp_mc_test_result',['isEmailPatientSent' => 1], ['ID_test_result' => $ID_test_result]);
	}
}

?>