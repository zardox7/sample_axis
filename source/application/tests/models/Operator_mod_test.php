<?php
ini_set('memory_limit', '1024M');
//		echo "\n";
class Operator_mod_test extends TestCase
{
	protected $ID_patient_created; 
	protected $ID_labtest_result_created; 

	function setUp(){
    	TestCase::resetInstance();
		$this->CI->load->model('Operator_mod');
		$this->obj = $this->CI->Operator_mod;
	}

	/**
	 * test function to Compares form values to the values in the database to perform authentication
	 *
	 *@test
	 */
	function login_operator(){
		$res = $this->obj->login_operator('admin@axis.com','123');
		$this->assertTrue($res);
	}

	/**
	 * test function that retrieves values from the database to populate option tags in lab test form dropdowns
	 *
	 *@test
	 */
	function get_options(){
		$options = $this->obj->get_options('lab');
		$options_first_keyname='';
		if(count($options) > 0) { 
			$options_first_keyname = key($options[0]);
		}
		$this->assertEquals('id',$options_first_keyname);
		$this->assertGreaterThan(0,count($options));

		$options = $this->obj->get_options();
		$this->assertEquals('id',$options_first_keyname);
		$this->assertGreaterThan(0,count($options));
	}

	/**
	 * test function get_links the HTML used in links can be used for any page
	 * Cannot get this to work because it relies on CI helper pagination
	 */
	 // *@test 
	function get_links(){
		$links = $this->createMock('Operator_mod',['get_links']);
		$links->method('get_links')->willReturn(8);
		$this->assertGreaterThan(6,$links->get_links());
	}

	/**
	 * test function get_page_data_patient Build SQL statement to return array of patient record data
	 *
	 *@test
	 */
	function get_page_data_patient(){
		$rec_array=$this->obj->get_page_data_patient(0, 10, '', '', 'ASC');
		$this->assertGreaterThan(0,$rec_array['total_rows']);
		$this->assertObjectHasAttribute('ID_patient',$rec_array['query_obj'][0]);
	}

	/**
	 * test function Submit a database request to get the field data for a single patient
	 *
	 *@test 
	 */
	function patient_get(){
		$rec_obj = $this->obj->patient_get('1001')->row();
		$this->assertEquals('Patient',$rec_obj->name_last);
		$this->assertObjectHasAttribute('ID_patient',$rec_obj);
		$this->assertObjectHasAttribute('name_last',$rec_obj);
	}

	/**
	 * test function Submit the user entered data from patient data on the add/update form to the database
	 *
	 *@test
	 */
	function patient_save(){	
		$patient_arr=[
			'ID_patient' => null,
			'patient_pass_code' => null,
			'name_first' => 'aaaa',
			'name_last' => 'bbbb',
			'email' => 'aaa@bbb.com',
			'address_1' => '22 Sunshine Way',
			'address_2' => '',
			'city' => 'Longview',
			'state' => 'WA',
			'postal_code' => '87645',
			'country' => 'USA',
			'gender' => 'M',
			'date_birth' => '1981-09-01'
		];
		$res = $this->obj->patient_save($patient_arr);
		$this->assertEquals(16,strlen($res['patient_pass_code']));
		$this->assertEquals('add',$res['type']);
		$this->assertGreaterThan(0,$res['ID_patient']);
		// update that patient
		$patient_arr['ID_patient']=$res['ID_patient'];
		$patient_arr['name_first']='cccccccccc';
		$patient_arr['name_last']='dddddddd';
		$res = $this->obj->patient_save($patient_arr);
		$this->assertEquals('update',$res['type']);

		// test patient_delete
		$res = $this->obj->patient_delete($res['ID_patient'],'all');
		$this->assertEquals(1,$res);
	}

	/**
	 * test function database request to delete a Patient record, and all records that are related by that Patient ID
	 *
	 *@test
	 */
	function patient_delete(){
		// tested in save
	}

	/**
	 * test function compile SELECT portion of the SQL statement
	 *
	 *@test
	 */
	function get_select_test_result_table(){
		$this->assertStringStartsWith('tr.ID_test_result',$this->obj->get_select_test_result_table());
	}

	/**
	 * test function to create SELECT portion of the SQL statement
	 *
	 *@test
	 */
	function get_from_test_result_table(){
		$this->assertStringEndsWith('as t ON (tr.FKID_test=t.ID_test)',$this->obj->get_from_test_result_table());
	}

	/**
	 * test function to compile SQL statement and retrieve test results record data
	 *
	 *@test
	 */
	function get_page_data_test_result(){
		$rec_array=$this->obj->get_page_data_test_result(0, 10, '1001', '', '', 'ASC');
		$this->assertGreaterThan(0,$rec_array['total_rows']);
		$this->assertObjectHasAttribute('ID_test_result',$rec_array['query_obj'][0]);
	}

	/**
	 * test function to to lookup all test result fields from the database
	 *
	 *@test
	 */
	function labtest_result_get(){
		$rec_array=$this->obj->get_page_data_test_result(0, 10, '1001', '', '', 'ASC');
		$obj_first=$rec_array['query_obj'][0];
		$this->assertGreaterThan(0,$obj_first->ID_test_result);
		$this->assertGreaterThan(0,$obj_first->FKID_patient);
	}

	/**
	 * test function to Submit the user entered data from test result data on the add/update form to the database
	 *
	 *@test
	 */
	function labtest_result_save(){
		$labtest_result_arr=[
			'ID_test_result' => null,
			'FKID_patient' => '1001',
			'FKID_test' => '1',
			'FKID_staff_lab' => '1',
			'FKID_lab' => '1',
			'date_test_result_request' => date("Y-m-d"),
			'date_test_result_service' =>  date("Y-m-d"),
			'test_result_diagnosis' => 'diagnosis goes here'
		];
		$res = $this->obj->labtest_result_save($labtest_result_arr);
		$this->assertEquals('add',$res['result']);
		$this->assertGreaterThan(0,$res['ID_test_result']);
		// update that labtest
		$labtest_result_arr['ID_test_result']=$res['ID_test_result'];
		$labtest_result_arr['test_result_diagnosis']='modified the diagnosis';
		$res = $this->obj->labtest_result_save($labtest_result_arr);
		$this->assertEquals('update',$res['result']);
		// test labtest_result_delete
		$res = $this->obj->labtest_result_delete($res['ID_test_result']);
		$this->assertEquals(1,$res);	
	}

	/**
	 * test function to Perform the database request to delete a record
	 *
	 *@test
	 */
	function labtest_result_delete(){
		// tested in save
	}

	/**
	 * test function to Perform the database request to find all field values to display on a report
	 *
	 *@test
	 */
	function patient_test_result_report(){
		$rec_obj = $this->obj->patient_test_result_report('7');
		$this->assertObjectHasAttribute('ID_test_result',$rec_obj);
		$this->assertEquals('7',$rec_obj->ID_test_result);
   	}

}
?>