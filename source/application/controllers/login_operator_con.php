<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Lab operator login controller
 * 
 * @package Lab Operator website
 * @subpackage Login
 * @author 	Michael Collins
 * @link	http://sample_axis.zardox.net/index.php/login_operator/
 */

/**
 * Login_operator_con class extends a wrapper function which in turn will do the extend of the CodeIgniter framework
* This class handles just the login function for lab staff / operators
 */
class Login_operator_con extends MY_Controller {

	function __construct() {
	    parent::__construct();
	}
	
	/**
	 * index - retrieves the operator login page
	 * 
	 * 
	 * @param     none
	 * @return none, outputs html to the page
	 */
	function index() {
		$data['staff_email']=get_cookie('staff_email');
		$content = $this->load->view('login_operator_view',$data,true);
		$this->render($content);
	}

    function logout(){
        $this->session->sess_destroy();
        redirect('login_operator_con');
    }

	/**
	 * Accept the login parameters entered by a lab operator and authenticates against database values
	 *
	 * @param none, uses form post values
	 * @return none, redirects to either the login form again, or to the main operator page
	 */
	function process_login_operator() {
		// validation of the login form
		$this->load->library('form_validation');
		$this->form_validation->set_rules('staff_email', 'Email', 'min_length[3]|max_length[24]|required');
		$this->form_validation->set_rules('staff_password', 'Password', 'min_length[3]|max_length[16]|required');
		// If validation fails, send the user back to the login page with an error message
		if ($this->form_validation->run() === false) {
			$val_errors = ['errors' => validation_errors()];
			$this->session->set_flashdata($val_errors);
			redirect('login_operator_con');
    	}
    	else {
			// the submit values are OK, next check in the database to authenticate this operator
			$staff_email = $this->input->post('staff_email');
			$staff_password = $this->input->post('staff_password');
			$this->load->model('operator_mod','',true);
			$res = $this->operator_mod->login_operator($staff_email,$staff_password);
			// This is not really necessary, but just make sure you have the staff_password prior to calling the get results constructor
			if($res === false){
				$this->session->set_flashdata(['errors' => 'Could not login']);
				redirect('login_operator_con');
			}
			else{
				// redirect to the operator main
				redirect('/operator_con/index/');
			}
		}
	}
}
?>