<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Lab Operator User Tasks controller
 * 
 * @package Lab Operator website
 * @author 	Michael Collins
 * @link	http://sample_axis.zardox.net/index.php/operator_con
 */

/**
 * Operator_con class extends a wrapper function which in turn will do the extend of the CodeIgniter framework
 */
class Operator_con extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('operator_mod','',true);
		$this->load->library('form_validation');
		$this->local_stylesheets = ['jquery-ui.css','sweetalert.css','select2.min.css']; 
		$this->local_javascripts = ['jquery-ui.js','sweetalert.min.js','select2.min.js']; 
	}
	
	/**
	 * Load the main page for the operator website, most of the other functions are AJAX that originate from this page
	 *
	 * @param none
	 * @return void output is the content of the page
	 */
	function index() {
		$content = $this->load->view('operator_main_view',NULL,true);
		$this->render($content);
	}
	
	/**
	 * AJAX method - request the values to be used in select dropdown as options on the list of choices
	 *
	 * @param none, use a post parameter to determine which select needs to be populated with options
	 * @return void output the JSON string to the page
	 */
	function ajax_get_option_json() {
		// Set array variables for the options to use in dropdown selectors for: tests, lab staff, and lab (location)
		if($this->input->is_ajax_request()) {
			$res_arr = $this->operator_mod->get_options($this->input->post('w'));
			header('Content-Type: application/json');
			if( is_array($res_arr) ){
				$output = json_encode($res_arr);
			}
			else{
				$output = '';
			}
			echo $output;
		}
	}
	
	/**
	 * match the format of the date to verify it is in a date format, used as a callback in form validation
	 *
	 * @param string $str the date to check
	 * @return boolean
	 */
	function valid_date($str) {
		$bool_res=false;
		if(preg_match ("/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/", $str, $parts)) {
			//check whether the date is valid of not
			if(checkdate($parts[2],$parts[1],$parts[3])){
				$bool_res=true;
			}
		}
		if( ! $bool_res) {
        	$this->form_validation->set_message('valid_date', 'The {field} field is not in the correct date format.');
        }
	}
	
	/**
	 *  AJAX method - Find the list of patients, allowing for a filter value, and present within a paged list
	 *
	 * @param  none     
	 * @return void Output the HTML to the page   
	 */
	function ajax_patient_get_paged_list() {
		if($this->input->is_ajax_request()) {
			$per_page = 10;
			$offset = $this->uri->segment(3);
			$patient_filter = $this->input->post('patient_filter');
			$sort_field = $this->input->post('sort_field');
			$sort_by = $this->input->post('sort_by');
			// run query
			$temp_arr = $this->operator_mod->get_page_data_patient($offset, $per_page, $patient_filter, $sort_field, $sort_by);
			if( $temp_arr === false) {
				$html = '<p class="text-danger">There were no patients found matching your search request.</p>';
			}
			else{
				(int)$total_rows = $temp_arr['total_rows'];
				$query_obj = $temp_arr['query_obj'];
				if( $total_rows > 0) {
					// generate table html
					$this->load->library('table');
					$this->table->set_template(array ( 'table_open'  => '<table id="id_patient_table" class="table table-bordered" style="padding-top:5px">' )); 
					$this->table->set_heading(
						'Name First', 
						'<div id="id_name_last_sort" class="sortable">Name Last</div>',
						'Email',
						'<div id="id_city_sort" class="sortable">City</div>',
						'<div id="id_state_sort" class="sortable">State</div>',
						'<div id="id_gender_sort" class="sortable">Gender</div>', 
						'<div id="id_date_birth_sort" class="sortable">Date of Birth (dd-mm-yyyy)</div>', 
						'Edit', 
						'View', 
						'Delete'
					);
					$i = 0;
					foreach( $query_obj as $patient ){
						$i++;
						$this->table->add_row(
							$patient->name_first, 
							'<a href="#" data-toggle="tooltip" title="'.$patient->patient_pass_code.'">'.$patient->name_last.'</a>', 
							mailto($patient->email), 
							$patient->city, 
							$patient->state, 
							strtoupper($patient->gender) == 'M' ? 'Male' : 'Female', 
							date('M j, Y',strtotime($patient->date_birth)),
							'<img id="id_load_'.$i.'" id_patient="'.$patient->ID_patient.'" src="'.base_url('assets/images/update.png').'" alt="update">',
							'<img id="id_view_'.$i.'" id_patient="'.$patient->ID_patient.'" src="'.base_url('assets/images/view.png').'" alt="view">',
							'<img id="id_delete_'.$i.'" id_patient="'.$patient->ID_patient.'" src="'.base_url('assets/images/delete.png').'" alt="delete">'
						);
					}
					// generate pagination
					$html = $this->operator_mod->get_links(
						[
							'base_url' => '',
							'total_rows' => $total_rows,
							'per_page' => $per_page,
							'num_links' => ceil( $total_rows / $per_page)
						]
					);
					// add the html for the table of results
					$html .= $this->table->generate();
				}
				else{
					$html = '<p class="text-danger">Some unknown error occurred.</p>';
				}
			}
			echo $html; 
		}
	}

	/**
	 *  AJAX method - JSON format results from a database query to load the patient update form with values
	 *
	 * @param integer $ID_patient ID of the patient
	 * @return void Output to the page is the JSON encoded string
	 */
	function ajax_patient_load($ID_patient) {
		if($this->input->is_ajax_request()) {
			if (isset($ID_patient) && ! empty($ID_patient)) { //Checks if action value exists
			$patient = $this->operator_mod->patient_get($ID_patient)->row();
			if(! $patient === false && (int)$patient->ID_patient > 0 ){
				$this->load->helper('MC');
				$json_data = json_encode(
					[
						'ID_patient' => $ID_patient,
						'patient_pass_code' => $patient->patient_pass_code,
						'name_first' => $patient->name_first,
						'name_last' => $patient->name_last,
						'email' => $patient->email,
						'address_1' => $patient->address_1,
						'address_2' => $patient->address_2,
						'city' => $patient->city,
						'state' => $patient->state,
						'postal_code' => $patient->postal_code,
						'country' => $patient->country,
						'gender' => $patient->gender,
						'date_birth' => $patient->date_birth,
						'patient_address_full' => address_block([
							'address_1' => $patient->address_1,
							'address_2' => $patient->address_2,
							'city' => $patient->city,
							'state' => $patient->state,
							'postal_code' => $patient->postal_code,
							'country' => $patient->country
						])
					]);
				}
				header('Content-Type: application/json');
				echo $json_data;
			}
		}
	}
	
	/**
	 *  AJAX method - validate form values then if in correct format, pass those values on for processing against the database, if there is ID_test_result, it is an update
	 *
	 * @param none, uses form parameter (POST) values
	 * @return void output a JSON encoded string with 4 fields: ID of patient, the pass code, the result of the save, and a message to be displayed
	 */
	function ajax_patient_save_process() {
		$ID_patient = 0;
		$patient_pass_code='';
		$result=0;
		$result_message='';
		if($this->input->is_ajax_request()) {
			$patient_post_array = $this->input->post('post_json');
			$this->form_validation->set_data($patient_post_array);
			$this->form_validation->set_rules('name_first', 'First Name', 'trim|required');
			$this->form_validation->set_rules('name_last', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('city', 'City', 'trim|required');
			$this->form_validation->set_rules('state', 'State', 'trim|required');
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
			$this->form_validation->set_rules('date_birth', 'Date of Birth', 'callback_valid_date');
			$this->form_validation->set_message('required', '{field} is required. ');
			$this->form_validation->set_error_delimiters('', ' ');
			// run validation
			if ($this->form_validation->run() === false){
				$result = 1;
				$result_message = $this->form_validation->error_string();
			}
			else{
				$patient_save_array = $this->operator_mod->patient_save($patient_post_array);
				$ID_patient = $patient_save_array['ID_patient'];
				if( $patient_save_array === false || empty($patient_save_array) ) {
					$result_message = 'Some unknown error occured. patient save failed';
				}
				elseif( empty($ID_patient) ){
					$result_message = 'Some unknown error occured. patient ID missing';
				}
				else{
					$patient_pass_code=$patient_save_array['patient_pass_code'];
					$result = 2;
					if( $patient_save_array['type'] == 'add') {
						$result_message = 'The new Patient was added successfully';
					}
					else{
						$result_message = 'Patient details were saved successfully';
					}
				}
			}
		}
		else{
			$result_message = 'Some unknown error occured. Not ajax';
		}		
		$json_data = json_encode([
			'ID_patient' => $ID_patient,
			'patient_pass_code' => $patient_pass_code,
			'result' => $result,
			'result_message' => $result_message
		]);
		header('Content-Type: application/json');
		echo $json_data;
	}
	
	/**
	 * AJAX method - Delete a single patient record 
	 *
	 * @param   integer $ID_patient the ID of the patient record
	 * @param   string $w Flag to indicate which records to delete
	 * @return  void - output to page is true or false (as a string)
	 */
	function ajax_patient_delete($ID_patient) {
		if($this->input->is_ajax_request()) {
			$w = $this->input->get('w');
			$res = $this->operator_mod->patient_delete($ID_patient,$w);
			echo $res;
		}
	}
	
	/**
	 * AJAX method - Find the list of test results for a patient and present within a paged list
	 *
	 * @param  none     
	 * @return  output the HTML to the page   
	 */
	function ajax_labtest_result_get_paged_list() {
		if($this->input->is_ajax_request()) {
			$per_page = 10;
			$offset = $this->uri->segment(3);
			$FKID_patient = $this->input->post('FKID_patient');
			$test_result_filter = $this->input->post('test_result_filter');
			$sort_field = $this->input->post('sort_field');
			$sort_by = $this->input->post('sort_by');
			// run query
			$temp_arr = $this->operator_mod->get_page_data_test_result($offset, $per_page, $FKID_patient, $test_result_filter, $sort_field, $sort_by);
			if( $temp_arr === false) {
				$html = '<p class="text-danger">There were no test results found.</p>';
			}
			else{
				(int)$total_rows = $temp_arr['total_rows'];
				$query_obj = $temp_arr['query_obj'];
				if( $total_rows > 0){
					// generate table html
					$this->load->library('table');
					$this->table->set_template(array ( 'table_open'  => '<table id="id_test_result_table" class="table table-bordered" style="padding-top:5px">' )); 

					$this->table->set_heading('<div id="id_tr_test_name_sort" class="sortable">Test</div>',
						'<div id="id_tr_staff_name_last_sort" class="sortable">Lab&nbsp;Staff</div>',
						'<div id="id_tr_lab_name_sort" class="sortable">Lab&nbsp;Name</div>',
						'<div id="id_date_test_result_request_sort" class="sortable">Date&nbsp;Request</div>',
						'<div id="id_date_test_result_service_sort" class="sortable">Date&nbsp;Service</div>', 
						'<div id="id_isEmailPatientSent_sort" class="sortable">Email&nbsp;Sent</div>', 
						'Edit', 
						'View', 
						'Delete'
					);
					$i = 0;
					foreach( $query_obj as $test_result ){
						$i++;
						$this->table->add_row(
							$test_result->test_name, 
							$test_result->staff_name_last, 
							$test_result->lab_name, 
							date('d-m-Y',strtotime($test_result->date_test_result_service)),
							date('d-m-Y',strtotime($test_result->date_test_result_request)),
							'<span id="id_'.$test_result->ID_test_result.'test_result_isEmailPatientSent">'.($test_result->isEmailPatientSent == '1' ? 'Yes' : 'No').'</span>', 
							'<img id="id_load_'.$i.'" id_test_result="'.$test_result->ID_test_result.'" src="'.base_url('assets/images/update.png').'" alt="update">',
							'<img id="id_view_'.$i.'" id_test_result="'.$test_result->ID_test_result.'" src="'.base_url('assets/images/view.png').'" alt="view">',
							'<img id="id_delete_'.$i.'" id_test_result="'.$test_result->ID_test_result.'" src="'.base_url('assets/images/delete.png').'" alt="delete">'
						);
					}
					// generate pagination
					$html = $this->operator_mod->get_links(
						[
							'base_url' => '',
							'total_rows' => $total_rows,
							'per_page' => $per_page,
							'num_links' => ceil( $total_rows / $per_page)
						]
					);
					// add the html for the table of results
					$html .= $this->table->generate();
				}
				else{
					$html = '<p class="text-danger">Some unknown error occurred.</p>';
				}
			}
			echo $html; 
		}
	}
	
	/**
	 * AJAX method - JSON format results from a database query to load the test result update form with values
	 *
	 * @param integer $ID_test_result the ID of the test result record
	 * @return output to the page is the JSON encoded string
	 */
	function ajax_labtest_result_load($ID_test_result) {
		if( ! $this->input->is_ajax_request()) {
			echo false;
		}
		elseif(isset($ID_test_result) && ! empty($ID_test_result)) { 
			$test_result = $this->operator_mod->labtest_result_get($ID_test_result)->row();
			if(! $test_result === false && (int)$test_result->ID_test_result > 0 ){
				$json_data = json_encode(
				[
					'ID_test_result' => $ID_test_result,
					'FKID_patient' => $test_result->FKID_patient,
					'FKID_test' => $test_result->FKID_test,
					'FKID_staff_lab' => $test_result->FKID_staff_lab,
					'FKID_lab' => $test_result->FKID_lab,
					'name_first' => $test_result->name_first, 
					'name_last' => $test_result->name_last, 
					'test_name' => $test_result->test_name, 
					'staff_name_first' => $test_result->staff_name_first, 
					'staff_name_last' => $test_result->staff_name_last, 
					'lab_name' => $test_result->lab_name, 
					'date_test_result_request' => $test_result->date_test_result_request,
					'date_test_result_service' => $test_result->date_test_result_service,
					'test_result_diagnosis' => $test_result->test_result_diagnosis,
					'isEmailPatientSent' => $test_result->isEmailPatientSent
				]);
			}
			header('Content-Type: application/json');
			echo $json_data;
		}
	}
	
	/**
	 *  AJAX method - validate form values then if in correct format, pass those values on for processing against the database
	 *
	 * @param 	integer $ID_test_result ID_test_result which can be 0, meaning do an add
	 * @return output a JSON encoded string with 4 fields: ID of test result, the foreign key ID of the patient, the result of the save, and a message to be displayed
	 */
	function ajax_labtest_result_save_process($ID_test_result=0) {
		$ID_test_result = 0;
		$FKID_patient = 0;
		$result='';
		$result_message='';
		if($this->input->is_ajax_request()) {
			$test_result_post_array = $this->input->post('post_json');
			$this->form_validation->set_data($test_result_post_array);
			$this->form_validation->set_rules('date_test_result_request', 'Date Test Result Request', 'callback_valid_date');
			$this->form_validation->set_rules('date_test_result_service', 'Date Test Result Service', 'callback_valid_date');
			$this->form_validation->set_rules('test_result_diagnosis', 'Diagnosis', 'trim|required');
			$this->form_validation->set_message('required', '{field} is required. ');
			$this->form_validation->set_error_delimiters('', ' ');
			// run validation
			if ($this->form_validation->run() === false){
				$result = 'fail';
				$result_message = $this->form_validation->error_string();
			}
			else{
				$test_result_save_array = $this->operator_mod->labtest_result_save($test_result_post_array);
				if(isset($test_result_save_array['ID_test_result'])){
					$ID_test_result = $test_result_save_array['ID_test_result'];
				}
				if(isset($test_result_save_array['FKID_patient'])){
					$FKID_patient = $test_result_save_array['FKID_patient'];
				}
				if( $test_result_save_array === false || empty($test_result_save_array) ) {
					$result_message = 'Some unknown error occured. test_result save failed';
				}
				elseif( empty($ID_test_result) ){
					$result_message = 'Some unknown error occured. test_result ID missing';
				}
				elseif( empty($FKID_patient) ){
					$result_message = 'Some unknown error occured. Patient ID missing';
				}
				else{
					$result = $test_result_save_array['result'];
					if( $test_result_save_array['result'] == 'add') {
						$result_message = 'The new test result was added successfully';
					}
					else{
						$result_message = 'The test result was saved successfully';
					}
				}
			}
		}
		else{
			$result_message = 'Some unknown error occured. Not ajax';
		}		
		$json_data = json_encode([
			'ID_test_result' => $ID_test_result,
			'FKID_patient' => $FKID_patient,
			'result' => $result,
			'result_message' => $result_message
		]);
		header('Content-Type: application/json');
		echo $json_data;
	}
	
	/**
	 * AJAX method - Delete a single test result record 
	 *
	 * @param   integer $ID_test_result ID of the test result record
	 * @return   string as true or false 
	 */
	function ajax_labtest_result_delete($ID_test_result) {
		if($this->input->is_ajax_request()) {
			$res = $this->operator_mod->labtest_result_delete($ID_test_result);
			echo $res;
		}
	}
	
	/**
	 * Pulls content from the database and formats it on a view page to produce a report of a patient and a test result
	 *
	 * @param   none
	 * @return  void string as true or false 
	 */
	function patient_test_result_report() {
		$ID_test_result = $this->uri->segment(3);
		if(isset($ID_test_result) && ! empty($ID_test_result)) {
			$data['patient'] = $this->operator_mod->patient_test_result_report($ID_test_result);
			$content = $this->load->view('patient_test_result_report',$data,true);
			$this->render($content);
		}
	}
	
	/**
	 * Operator option to send an email to the patient
	 *
	 * @return   void  
	 */
	function ajax_send_email() {
		$ID_test_result = $this->input->post('ID_test_result');
		$patient_pass_code = $this->input->post('patient_pass_code');
		$email_pdf = $this->input->post('email_pdf');
		$this->load->model('patient_mod','',true);
		if ( ! $ID_test_result ){
			$val_errors = 'Identifier is missing. ';
    	}
 		elseif ( ! $patient_pass_code ){
			$val_errors = 'Patient Pass Code is missing. ';
    	}
  		elseif ( ! $email_pdf ){
			$val_errors = 'Patient Email is missing. ';
    	}
     	else {
		    $err_arr =  $this->patient_mod->send_test_result_email($ID_test_result,$patient_pass_code,$email_pdf);
		    $val_errors = implode($err_arr);
		}
		echo $val_errors;
	}

	
	/**
	 * Operator option to open a pdf of the test result
	 *
	 * @return   void  
	 */
	function open_pdf() {
		$ID_test_result = $this->input->post('ID_test_result');
		$this->load->model('patient_mod','',true);
		if ( ! $ID_test_result ){
			echo 'Identifier is missing. ';
    	}
     	else {
		    $err_arr =  $this->patient_mod->send_test_result_email($ID_test_result,$patient_pass_code,$email_pdf);
		    $val_errors = implode($err_arr);
		}
	}

	/**
	 * Run a maintenance SQL statement to delete test result records that have no parent patient record
	 *
	 * @return   void  
	 */
	function run_Maintenance(){
		echo $this->operator_mod->maintenance_delete_orphan_test_result_records();
	}
}
?>