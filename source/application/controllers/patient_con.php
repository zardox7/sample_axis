<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Patient User Tasks controller
 * 
 * @package Patient website
 * @author 	Michael Collins
 * @link	http://sample_axis.zardox.net/index.php/patient_con/
 */

/**
 * Operator_con class extends a wrapper function which in turn will do the extend of the CodeIgniter framework
 */
class Patient_con extends MY_Controller {

	function __construct() {
	    parent::__construct();
		$this->load->model('patient_mod','',true);
		$this->load->library('table');
		// $this->output->enable_profiler( ! $this->input->is_ajax_request());
	}

	/**
	 * Handle calls to the root location of the patient website, redirects to login if there is no passcode stored in a session
	 *
	 * @param  none, patient_pass_code is passed as a session variable
	 * @return none    
	 */
	function index() {
		$patient_pass_code = $this->session->userdata('patient_pass_code');
		if ( empty($patient_pass_code) ) {
			$this->session->set_flashdata(['errors' => 'There is no patient pass code']);
			redirect('/login_patient_con/');
		}
		else{
			redirect('/patient_con/get_results/'.$patient_pass_code);
		}
	}
	
	/**
	 * Finds the record data needed to populate the patient detail view page
	 *
	 * @param string $patient_pass_code the patient pass code acts as a password and identifier of the record, since it is unique
	 * @return outputs the HTML for the viewable page
	 */
	function get_results($patient_pass_code) {		
		// If no patient pass code passed to this function, attempt to get it from the session
		if ( empty($patient_pass_code) ) {
			$patient_pass_code = $this->session->userdata('patient_pass_code');
    	}
		// If no patient pass code from the session then go back to login
		if ( empty($patient_pass_code) ) {
			$this->session->set_flashdata(['errors' => 'There is no patient pass code']);
			redirect('/login_patient_con/');
		}
		else{
			// data contains variables for use on view page
			// Get data about the patient
			$data = $this->patient_mod->get_patient($patient_pass_code);
			// num_test_results determines if results are shown on view page
			$data['num_test_results']=false;
			$data['date_range_results']=null;
			if($data === false){
				log_message('debug','patient_con/get_results - Patient could not be found get_patient no results');				
			}
			elseif( ! array_key_exists('patient_pass_code', $data) || is_null($data['patient_pass_code'])) {
				log_message('debug','patient_con/get_results - Patient could not be found patient_pass_code not contained in results');				
			}
			else{
				$patient_pass_code=$data['patient_pass_code'];
				// SET Value for date_range_results variable
				// If a value for the date range to be used is passed from a form, use that
				if( $this->input->post('date_range_results') != false) {
					$date_range_results = $this->input->post('date_range_results');
				}
				// otherwise if there is a date range set in the session, use that
				elseif( ! empty($this->session->userdata('date_range_results'))){
					$date_range_results = $this->session->userdata('date_range_results');
				}
				// use a default of 6 months for the date range
				else{
					$date_range_results = '6';
				}
				// set the session value for the date range to be used
				$this->session->set_userdata(['date_range_results' => $date_range_results]);
				$data['date_range_results']=$date_range_results;
						
				// Find all test results within the specified date range
				$query_obj= $this->patient_mod->get_patient_test_results($patient_pass_code,$date_range_results);
				if(! isset($query_obj)) {
					log_message('debug','patient_con/get_results - query_obj NOT SET');				
				}
				elseif(! is_object($query_obj)) {
					log_message('debug','patient_con/get_results - query_obj NOT an object');				
				}
				elseif($query_obj->num_rows() > 0) {
					$data['num_test_results']=$query_obj->num_rows();
					// Build table to display listing of test results
		 		   	$this->table->set_template(array ( 'table_open'  => '<table class="table table-bordered" style="padding-top:5px">' )); 
					$this->table->set_heading('Test Code','Test Name','Date Request', 'Date Service','Lab Operator Name','Lab Name','Lab Location');
					foreach ($query_obj->result_array() as $test_row) {
						$this->table->add_row(
							$test_row['test_code'],
							'<a href="'.site_url('patient_con/get_a_test_result/'.$test_row['ID_test_result'].'/'.$patient_pass_code).'/x">'.$test_row['test_name'].'</a>',
							date('d-m-Y',strtotime($test_row['date_test_result_request'])),
							date('d-m-Y',strtotime($test_row['date_test_result_service'])),
							($test_row['staff_name_first'].' '.$test_row['staff_name_last']),
							$test_row['lab_name'],
							$test_row['lab_location']
						);
					}
				}
				else{
					log_message('debug','patient_con/get_results - did not find any test results');				
				}
			}
			$this->load->helper('MC');
			$data2 = ['address_block' => address_block([
					'address_1' => $data['address_1'],
					'address_2' => $data['address_2'],
					'city' => $data['city'],
					'state' => $data['state'],
					'postal_code' => $data['postal_code'],
					'country' => $data['country']
				])
			];
			$data = array_merge($data, $data2);
			$content = $this->load->view('test_result_listing_view',$data,true);
			$this->render($content);
		}
	}
	
	/**
	 * Finds the record data needed to populate the patient test result detail view page
	 *
	 * @param string $patient_pass_code For currently signed on user	 
	 * @param integer $ID_test_result ID for the test result to be displayed 
	 * @param string a flag to indicate if the view is a PDF formatted page or the HTML
	 * @return outputs the HTML for the viewable page
	 */
	function get_a_test_result($ID_test_result,$patient_pass_code,$pdf) {
		if( empty($ID_test_result)) {
			log_message('error','patient_con/get_a_test_result - ID_test_result parameter not passed to page');	
			die;			
		}
		elseif( empty($patient_pass_code)) {
			log_message('error','patient_con/get_a_test_result - patient_pass_code parameter not passed to page');	
			die;			
		}
		else{
			$this->load->helper('MC');
			$data1 = $this->patient_mod->get_patient($patient_pass_code);
			$data2 = $this->patient_mod->get_a_test_result($ID_test_result);
			$data3 = ['address_block' => address_block([
					'address_1' => $data1['address_1'],
					'address_2' => $data1['address_2'],
					'city' => $data1['city'],
					'state' => $data1['state'],
					'postal_code' => $data1['postal_code'],
					'country' => $data1['country']
				])
			];
			if( ! (isset($data1) && is_array($data1))) {
				log_message('error','patient_con/get_a_test_result - data1 patient array NOT SET');	
				die;			
			}
			elseif( ! (isset($data2) && is_array($data2))) {
				log_message('error','patient_con/get_a_test_result - data2 test results array NOT SET');	
				die;			
			}
			elseif($pdf == 'pdf'){
				$data = array_merge($data1, $data2, $data3);
				return $this->load->view('test_result_report_pdf_view',$data,true);
			}
			else{
				$data = array_merge($data1, $data2, $data3);
				$content = $this->load->view('test_result_detail_view',$data,true);
				return $this->render($content);
			}
		}
	}

	/**
	 * Generate a PDF version the test result detail using TCPDF third-party helper
	 *
	 * @param string $patient_pass_code For currently signed on user	 
	 * @param integer $ID_test_result ID for the test result to be displayed 
	 * @return output the PDF (into a new window opened by jQuery function)
	 */
	function pdf($ID_test_result,$patient_pass_code) {
	    $this->load->helper('pdf');
		$data = $this->get_a_test_result($ID_test_result,$patient_pass_code,'pdf');
		tcpdf();
		$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Pathology Testing Lab Report";
		$obj_pdf->SetTitle($title);
		$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		$obj_pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
		$obj_pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->AddPage();
		ob_start();
		    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->writeHTML($data, true, false, true, false, '');
		$obj_pdf->Output('output.pdf', 'I');
	}

	/**
	 * Send an email using PHPMAILER third-party helper to the user supplied email address
	 *
	 * @param string $patient_pass_code For currently signed on user	 
	 * @param integer $ID_test_result ID for the test result to be displayed 
	 * @return none, the page is redirected to the test result detail page 
	 */
	function email() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email_pdf', 'Email Address', 'min_length[6]|max_length[100]|required|valid_email');
		// If validation fails, send the user back to the login page with an error message
		if ($this->form_validation->run() === false) {
			$val_errors =['errors_email' => validation_errors()];
    	}
    	else {
			$ID_test_result = $this->input->post('ID_test_result');
			$patient_pass_code = $this->input->post('patient_pass_code');
			$email_pdf = $this->input->post('email_pdf');
		    $val_errors = ['errors_email' => $this->patient_mod->send_test_result_email($ID_test_result,$patient_pass_code,$email_pdf)];
		}
		$this->session->set_flashdata($val_errors);
		redirect('patient_con/get_a_test_result/'.$ID_test_result.'/'.$patient_pass_code.'/x');
	}
}
?>