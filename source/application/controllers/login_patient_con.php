<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Patient login controller
 * 
 * @package Patient website
 * @subpackage Login
 * @author 	Michael Collins
 * @link	http://sample_axis.zardox.net/index.php/login_patient_con/
 */


/**
 * Login_patient_con class extends a wrapper function which in turn will do the extend of the CodeIgniter framework
 * This class handles just the login function for patients
 */
class Login_patient_con extends MY_Controller {

	function __construct() {
	    parent::__construct();
	}
	
	/**
	 * index - retrieves the patient login page
	 * 
	 * 
	 * @param     none
	 * @return none, outputs html to the page
	 */
	function index() {
		$data['patient_pass_code']='';
		if( ! empty($this->input->post('patient_pass_code'))){
			// This needs to be revised , the idea is to grab the last parameter from a URL such as: websitte.com/get_results/IJHDYDJKFEJD 
			// (as long as the second parameter is "get_results""). That is the URL given to customers in the email sent by the lab
			$data['patient_pass_code']=$this->input->post('patient_pass_code');
		}
		else{
			// If the user did not pass any parameter but is making a return visit to this site, the passcode comes from a cookie set on that previous visit
			$data['patient_pass_code']=get_cookie('patient_pass_code');
		}
		// display the login form, pass any value there might be for the patient_pass_code
		// the user still needs to submit that login page, adding their last name to the form
		$content = $this->load->view('login_patient_view',$data,true);
		$this->render($content);
	}

	/**
	 * Accept the login parameters entered by a patient user and authenticates against database values
	 *
	 * @param none, uses form post values
	 * @return none, redirects to either the login form again, or to the main Patient page
	 */
	function process_login_patient() {
		// validation of the login form
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name_last', 'Last Name', 'min_length[3]|max_length[24]|required');
		$this->form_validation->set_rules('patient_pass_code', 'Passcode', 'min_length[3]|max_length[16]|required');
		// If validation fails, send the user back to the login page with an error message
		if ($this->form_validation->run() === false) {
			$val_errors = ['errors' => validation_errors()];
			$this->session->set_flashdata($val_errors);
			redirect('login_patient_con');
    	}
    	else {
			// the submit values are OK, next check in the database to authenticate this patient
			// only the last name is being used to reduce confusion over varying spellings of the first names
			$name_last = $this->input->post('name_last');
			$patient_pass_code = $this->input->post('patient_pass_code');
			$this->load->model('patient_mod','',true);
			$patient_pass_code = $this->patient_mod->login_patient($name_last,$patient_pass_code);
			// This is not really necessary, but just make sure you have the patient_pass_code prior to calling the get results constructor
			if($patient_pass_code === false){
				$this->session->set_flashdata(['errors' => 'Could not retrieve the passcode']);
				redirect('login_patient_con');
			}
			else{
				// redirect to the patient test results constructor
				redirect('/patient_con/get_results/'.$patient_pass_code);
			}
		}
	}
}
?>