Michael Collins
September 2016

Axis Pathology Lab Reporting System

_______________________________
Technical Modifications
_______________________________

This solution uses the CodeIgniter framework. Modify the settings in application/config/config.php to a value to the path and domain you will use to run this solution. On line 26, change the host domain: 

$config['base_url'] = 'http://sample_axis.zardox.net/';

to:

$config['base_url'] = 'http://{YOUR WEB SERVING DOMAIN OR VIRTUAL HOST}/';

Change the path to root of your file system on line 373: 

$config['sess_save_path'] = '/Users/zardox/Sites/sample_axis/source/application/logs';

to:

$config['sess_save_path'] = '{YOUR WEB SERVING DIRECTORY}/{DIRECTORY NAME OF SOLUTION}/application/logs';

For the creation of PDF files of the lab result report, the location of the logo image needs to be set in "application/helpers/tcpdf/config/tcpdf_config.php" on line 66:

define ('K_PATH_IMAGES', '/Users/zardox/Sites/sample_axis/source/assets/images/');

Change the path to root of your file system.

And for email, the SMTP setting for your email server is set in the file "application/helpers/phpmailer_helper.php": 

	define ('PHPMAILER_PORT', 1025);
	define ('PHPMAILER_HOST', 'localhost');
	define ('PHPMAILER_FROM_EMAIL', 'MAIL@zardox.net');
	define ('PHPMAILER_FROM_NAME', 'admin');
	define ('PHPMAILER_SMTPAUTH', false);
	define ('PHPMAILER_USERNAME', '');
	define ('PHPMAILER_PASSWORD', '');
	
Note that the PHPMAILER_USERNAME and PHPMAILER_PASSWORD values need only be set if PHPMAILER_SMTPAUTH is set to true.

_______________________________
Database
_______________________________

A MySQL script to create the database and sample records is in a separate file at the root of the solution folder:

CREATE_DATABASE.mysql

This was created with mysqldump. The database name is "sample_axis", change that to preferred name and edit the creation scipt to use that alternate name. 

In the CodeIgniter framework application/config directory, edit the file "database.php" to indicate the database used to run this solution. In this section of the file:

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'mc',
	'database' => 'sample_axis',
	...

See the file "data dictionary.html" for a list of tables and fields in the database.
	
_______________________________
Web Site URL
_______________________________	
	
The default Web site page is the login for a patient (if there are no URL parameters):

	http://{YOUR WEB SERVING HOST}/
	
Also, any URL that begins with either:	
	
	http://{YOUR WEB SERVING HOST}/index.php/patient
	http://{YOUR WEB SERVING HOST}/index.php/login_patient_con

The browser is directed to the patient login page.

And, if any URL begins with either:

	http://{YOUR WEB SERVING HOST}/index.php/operator
	http://{YOUR WEB SERVING HOST}/index.php/login_operator_con

The browser is directed to the Lab Operator/Staff login page.

_______________________________
Definitions
_______________________________

Operator = Pathology lab staff.

Report = Medical test result report, including a selection of tests for a single patient.

Lab Test = A medical diagnotic test performed at the request of a Physician, Nurse, or a Patient. There are many tests grouped for view on a report.

_______________________________
Assumptions
_______________________________

I made the following assumptions, though typically I would confer with the client to understand better what their needs were. 

[A] Passcode access to tests

Is the patient passcode tied to a specific test or to all tests for a particular patient? And thus is a data element for the "exp_mc_patient" table. The passcode would need to be associated with any given test, if the Operator or Physician would not want the patient to view certain tests or when a report should not be available to view until all tests are completed. I assumed the patient should be able to access all tests on a single report and that if multiple tests are run, the passcode for that patient is the same on each email sent to that patient, and the emails are sent as each operator completes their work. For example, a patient had several tests done after seeing a Physician, the operator(s) completed their work on 3 tests, and then there will are 3 emails sent:

Jane Doe, passcode: 8IU7HJjgh5eOI7yQ - Blood pathology; date requested: 3/4/2016; completed: 3/5/2016
Jane Doe, passcode: 8IU7HJjgh5eOI7yQ - X-ray; date requested: 3/4/2016; completed: 3/4/2016
Jane Doe, passcode: 8IU7HJjgh5eOI7yQ - ECG; date requested: 3/4/2016; completed: 3/4/2016

Each of these emails lead to the same report. In the example above, the report would show results on Blood pathology, X-ray, and ECG.

[B] Determination of complete report

It seems unavoidable to have separate emails sent for each test result, as operators are likely not to know the status and completeness of other tests (if any). However, the client may later refine this system to include a signifier (flag field) that shows if all tests are complete and have the last operator who enters a test manually indicate that all tests done. Or, have a programmatic check on all tests for a patient (and if all are marked complete a single email is sent), but if so, there must be a process in which all requested tests are entered at one time (and the status shown as "incomplete" if an operator has not yet done the work on one of the tests). That initial entry would perhaps be done by the Physician (after examination the Physician is the one to initiate the required series of tests into the system). The role of the Physician is not discussed in the requirements (and there may not be a Physician if the lab accepts walk-in patients who perhaps just want to get their blood pressure and cholesterol, for example).

[C] Difference between reports and tests

The requirements specify that the "Reports" should contain multiple "Tests" (and results) and discuss the creation and updating of reports as data elements. However, I have defined the terms such that reports are generated only on-the-fly, they do not exist as a data element, but exist only when a grouping of tests are presented to a Web page or PDF (and would have verified this with a client if I had one). Thus, it is "Tests" which are created, updated, deleted. The report is a momentary collection of "Tests" which is typically filtered by date.

It could be that the date of examination (or date of test request) be used to group tests into a report. Yet, a hospitalised patient, with a multiple day hospital stay, could not view all test results on one report, but would have to view the reports per day. There are many medical conditions that treatment and testing occurs over multiple days.

Jane Doe

Available reports:

Report 3/4/2016 - 
	Blood pathology
	X-ray
	ECG

Report 3/5/2016 - 
	Blood pressure (10 am)
	Urinalysis
	Pneumocystis carinii PCR
	Blood pressure (4 pm)
	EKG

Report 3/6/2016 - 
	Blood pathology
	Blood pressure
	Reticulocyte count

It could also be that a "Report" is an analysis of a number of tests and thus the grouping of tests happens at the reporting stage. For example, a Physician calls up the patient's records, test results, makes a single diagnosis on the set of tests and thus groups all into a single definable unit (the report). However, the specifications do not address any diagnosis/analysis stage in the system so I have kept it simple.

Thus, the requirement to display a list of a Patient's reports is rather a list of their tests since there is no clear means to group tests into a report. I have decided that a range of dates (date of examination or test request date) is necessary to select the tests included in a report. The default is all dates for the past six months. 

[D] Entity relationships

A test_result is defined to have only one test, operator, physician, and patient. This business rule should be confirmed with the client.

The "test" table is a key identifier for a generic listing of various tests.

A "exp_mc_staff" table is defined so that it could be used for several roles: Lab operator, physician, clerical, or other hospital staff that is involved in this system.

ID values are never revealed on forms, they exist for database use only. "Code" values identify elements with a reference that is visible to the user.

I have assumed that all the test result can be summed up in a single text field. In a real world case I would perhaps suggest, or need to, take the diagnosis field and make that a reference to another set of tables that defines labels and entries so that all elements of a lab test can be defined. A test result form is likely to have a complex set of parameters which could be defined in a meta table of labels and entries. I have not attempted to take this solution to that level of refinement.

_______________________________
Login - Operator / Physician
_______________________________

Functions:

	• Email and password entry fields

		> Exact matches required

_______________________________
Patient Functions
_______________________________

Login:

	• Name and passcode entry fields.

		> Passcode is an exact match.
		> Entry of the name should auto-complete based on records from database.
		
NOTE: A stated requirement is to have an auto-complete of patient name, but I did not implement that since it is a breach of patient privacy as names of patients are revealed.

Tasks:
	• List Tests as a Report (include filter for dates, with last 6 months as default).
	• Display a detail view of a Test Result.
	• Export a report as PDF (using PDF export library).
	• Mail a report as PDF (using PHP Mailer).

_______________________________
Lab Operator Functions 
_______________________________

After login, a Lab Operator views a "Dashboard" (lists of tasks they can complete)

From Dashboard:
	• Create Patient
	• Search Patients
	• List Patients (link to View Patient Details)
	• Filter list
		
From Patient Detail:
	• Update Patient Details
	• Delete Patient Details and Tests (not the Patient) 
	• Delete Patient (and all Details and Tests) 

From Lab Test Result List :
	• List Tests for a Patient (links to Test Result Detail)
	• Filter by date ranges (1 month, 6 months, 1 year, and all)
	• Add Test Result (a Patient must be found, or new one added, prior to enter Test Results)

From Lab Test Result Detail :
	• Send text message to patient (provide test name and pass code). If no email, display option to print mailing form. A flag is set to indicate that the email has been sent by the operator (at least one time)
	• Update Test Result
	• Delete Test Result
	• Display PDF of the Lab Test detail

_______________________________
Report content
_______________________________

Editable by Operator on report details page:

date_test_result_request
date_test_result_service
test_result_diagnosis

Display (from joins)

exp_mc_staff.staff_role 
exp_mc_staff.staff_name_first
exp_mc_staff.staff_name_last
exp_mc_staff.staff_location

exp_mc_patient.name_first
exp_mc_patient.name_last
exp_mc_patient.date_birth
exp_mc_patient.address_street
exp_mc_patient.address_city
exp_mc_patient.address_state
exp_mc_patient.address_postcode
exp_mc_patient.patient_pass_code

exp_mc_lab.lab_code
exp_mc_lab.lab_name
exp_mc_lab.lab_location

exp_mc_test.test_name
exp_mc_test.test_code

_______________________________
List of Technologies 
_______________________________
• PHP
• Bootstrap (for CSS uniformity, layout of pages, and ease of implementation)
• JQuery
• JQuery UI (for auto-completion and date picker)
• Select2 (JQuery extension for improving select functions in forms)
• TCPDF (PDF export library for dynamic creation of PDF files)
• PHPMailer (library for sending of emails)
• Sweetalert (JQuery extension for dialogs)

_______________________________
Design Patterns
_______________________________

The overall project is divided into 2 overall functions: for a patient user and for the administrative use of a Lab Operator. The Patient user web site uses standard form submit approach to load various php scripts / web pages. The operator web site loads one main script with PHP but then all interactivity with that page is via JavaScript/JQuery. It utilizes AJAX to call various PHP scripts to process the requested events initiated by the user of the site. The only time that a separate page is loaded is to display a PD or HTML view of a Lab test result.

As far as the UI design patterns, the Web pages created in this Web application rely on Bootstrap for laying out of pages, providing CSS for improving the look of various elements of the page, and providing somewhat standard Web page components.


